using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Diagnostics;
using System;

public class WEB : MonoBehaviour
{
    //////////////////////////////
    #region /// Public properties/fields
    public Arms.ArmsClass _Arms { get; set; }
    public Facts.IntrestingFactsClass _Facts { get; set; }
    public Test.TestClass _Test { get; set; }
    public Settings _Settings { get; set; }
    #endregion
    //////////////////////////////


    //////////////////////////////
    #region /// Private properties/fields
    static readonly Logger logger = new Logger("WebLogger");
    AGOSH.Weber webClient;
    private int currentImage = 0;
    #endregion
    //////////////////////////////


    //////////////////////////////
    #region /// Public functions
    public IObservable<Unit> Connect()
    {
        logger.DebugFormat( "{0}    =>    {1}\n", this, "start Connect" );

        // ������ ������������
        return GetConfig<Settings>( AGOSH.Helper.GetResourcesPath() + "Config.xml", logger )
            .CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )
            // ���������
            .Do( settingsClass =>
            {
                _Settings = settingsClass;

                webClient = AGOSH.Weber.Create()
                .Url( _Settings.url )
                .Login( _Settings.login )
                .Password( _Settings.pass )
                .Build();
            } )
            //// ��������������
            //.SelectMany( _ => Authenticat( webClient, logger ) )
            //.CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )
            ////���������� xml
            //.SelectMany( _ => GetStructure( _Settings.catalogTest, AGOSH.Helper.GetFullPath() + "Structure Test.xml", logger ) )
            //.CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )

            //.SelectMany( _ => GetStructure( _Settings.catalogArms, AGOSH.Helper.GetFullPath() + "Structure Arms.xml", logger ) )
            //.CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )

            //.SelectMany( _ => GetStructure( _Settings.catalogFacts, AGOSH.Helper.GetFullPath() + "Structure Facts.xml", logger ) )
            //.CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )

            // ������ xml � ����������������
            .SelectMany( _ => GetConfig<Test.TestClass>( AGOSH.Helper.GetFullPath() + "Structure Test.xml", logger ) )
            .CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )
            .Do( newClass => _Test = newClass )

            .SelectMany( _ => GetConfig<Arms.ArmsClass>( AGOSH.Helper.GetFullPath() + "Structure Arms.xml", logger ) )
            .CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )
            .Do( newClass => _Arms = newClass )

            .SelectMany( _ => GetConfig<Facts.IntrestingFactsClass>( AGOSH.Helper.GetFullPath() + "Structure Facts.xml", logger ) )
            .CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )
            .Do( newClass => _Facts = newClass )

            .AsUnitObservable();
    }

    public IObservable<Unit> LoadTestImages( IProgress<int> reportProgress )
    {
        currentImage = 0;
        List<Test.ImageClass> imagesTest = new List<Test.ImageClass>();
        foreach( var qizUnit in _Test.QizUnit )
        {
            foreach( var question in qizUnit.Questions )
            {
                if( question.Data.Image != null )
                    imagesTest.Add( question.Data.Image );
            }
        }

        logger.DebugFormat( "{0}    =>    {1}\n", this, "start LoadTestImages" );
        return Observable.Range( 0, imagesTest.Count )
            .SelectMany( i => GetImage( imagesTest[ i ].Id, AGOSH.Helper.GetFullPath() + "image" + imagesTest[ i ].Id + ".jpg", logger ) )
            .CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )
            .Do( _ =>
            {
                currentImage = currentImage + 1;
                if( reportProgress != null )
                {
                    try
                    {
                        reportProgress.Report( (int)(currentImage * 100 / imagesTest.Count) );
                    }
                    catch( Exception ex )
                    {
                        Observable.Throw<Unit>( ex );
                    }
                }
            } )
            .Last()
             .Do( _ => logger.DebugFormat( "{0}    =>    {1}\n", this, "done LoadTestImages" ) )
            .AsUnitObservable();
    }
    
    public IObservable<Unit> LoadArmorImages( IProgress<int> reportProgress )
    {
        logger.DebugFormat( "{0}    =>    {1}\n", this, "start LoadArmorImages" );

        currentImage = 0;
        List<Arms.ImageClass> imagesArmsThumb = new List<Arms.ImageClass>();
        List<Arms.ImageClass> imagesArmsImage = new List<Arms.ImageClass>();
        foreach( var typeOfArm in _Arms.TypeOfArms )
        {
            foreach( var arm in typeOfArm.Arms )
            {
                if( arm.Data.Images != null )
                {
                    if( arm.Data.Images[ 1 ] != null ) imagesArmsThumb.Add( arm.Data.Images[ 1 ] );
                    if( arm.Data.Images[ 0 ] != null ) imagesArmsImage.Add( arm.Data.Images[ 0 ] );
                }
            }
        }

        IObservable<Unit> asyncRequest_1 = null;
        IObservable<Unit> asyncRequest_2 = null;
        IObservable<Unit> seq_1 = null;
        IObservable<Unit> seq_2 = null;

        asyncRequest_1 = Observable.Return( Unit.Default ).Do( _ => logger.DebugFormat( "{0}    =>    {1}\n", this, "LoadThumbs" ) )
            .SelectMany( _ => Observable.Range( 0, imagesArmsThumb.Count ) )
            .SelectMany( i => GetImage( imagesArmsThumb[ i ].Id, AGOSH.Helper.GetFullPath() + imagesArmsThumb[ i ].Name + imagesArmsThumb[ i ].Id + ".jpg", logger ) )
            .CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )
            .Do( _ =>
            {
                currentImage = currentImage + 1;
                if( reportProgress != null )
                {
                    try
                    {
                        reportProgress.Report( (int) ( currentImage * 50 / imagesArmsThumb.Count) );
                    }
                    catch( Exception ex )
                    {
                        Observable.Throw<Unit>( ex );
                    }
                }
            } )
            .Last()
            .Do( _ => logger.DebugFormat( "{0}    =>    {1}\n", this, "done LoadThumbs" ) )
            .AsUnitObservable();

        asyncRequest_2 = Observable.Return( Unit.Default ).Do( _ => logger.DebugFormat( "{0}    =>    {1}\n", this, "LoadImages" ) )
            .SelectMany( _ => Observable.Range( 0, imagesArmsImage.Count ) )
            .SelectMany( i => GetImage( imagesArmsImage[ i ].Id, AGOSH.Helper.GetFullPath() + imagesArmsImage[ i ].Name + imagesArmsImage[ i ].Id + ".jpg", logger ) )
            .CatchIgnore( ( Exception ex ) => logger.ErrorFormat( "{0}    =>    {1}\n", this, ex ) )
            .Do( _ =>
            {
                currentImage = currentImage + 1;
                if( reportProgress != null )
                {
                    try
                    {
                        reportProgress.Report( (int) ( currentImage * 50 / imagesArmsImage.Count) );
                    }
                    catch( Exception ex )
                    {
                        Observable.Throw<Unit>( ex );
                    }
                }
            } )
            .Last()
            .Do( _ => logger.DebugFormat( "{0}    =>    {1}\n", this, "done LoadImages" ) );

        seq_1 = asyncRequest_1.PublishLast().RefCount();
        seq_2 = asyncRequest_2.PublishLast().RefCount();

        return seq_1.SelectMany( _ => seq_2 ).Do( _ => logger.DebugFormat( "{0}    =>    {1}\n", this, "done LoadArmorImages" ) );
    }    
    #endregion
    //////////////////////////////


    //////////////////////////////
    #region /// Private functions
    IObservable<Unit> GetImage( string imageId, string imageFilePatch, Logger logger )
    {
        logger.DebugFormat( "{0}    =>    {1}  |  {2}\n", this, "start GetImage", imageId );

        IObservable<Unit> asyncRequest = null;
        asyncRequest = AGOSH.Weber.Create().Build().DownloadFileAsObservable( imageId, imageFilePatch )
            .Catch( ( Exception error ) => Observable.Throw<Unit>( error ) )
            .AsUnitObservable();

        return asyncRequest.PublishLast().RefCount().Do( _ => logger.DebugFormat( "{0}    =>    {1}  |  {2}\n", this, "done GetImage", imageId ) );
    }

    IObservable<Unit> GetStructure( string structureId, string structureFilePatch, Logger logger )
    {
        logger.DebugFormat( "{0}    =>    {1}  |  {2}\n", this, "start GetStructure", structureId );

        IObservable<Unit> asyncRequest = null;
        asyncRequest = AGOSH.Weber.Create().Build().DownloadStructureAsObservable( structureId, structureFilePatch )
            .Catch( ( Exception error ) => Observable.Throw<Unit>( error ) );

        return asyncRequest.PublishLast().RefCount().Do( _ => logger.DebugFormat( "{0}    =>    {1}\n", this, "done GetStructure" ) );
    }

    IObservable<T> GetConfig<T>( string configFilePatch, Logger logger )
    {
        logger.DebugFormat( "{0}    =>    {1}  |  {2}\n", this, "start GetConfig", configFilePatch );

        IObservable<T> asyncRequest = null;
        asyncRequest = AGOSH.Serializer.DeserializeAsObservable<T>( configFilePatch )
            .Catch( ( Exception error ) => Observable.Throw<T>( error ) )
            .SelectMany( result =>
            {
                if( result == null )
                    return Observable.Throw<T>( new Exception( "result is null" ) );
                else
                    return Observable.Return( result );
            } );

        return asyncRequest.PublishLast().RefCount().Do( _ => logger.DebugFormat( "{0}    =>    {1}\n", this, "done GetConfig" ) );
    }

    IObservable<Unit> Authenticat( AGOSH.Weber webClient, Logger logger )
    {
        logger.DebugFormat( "{0}    =>    {1}\n", this, "start Authenticat" );

        IObservable<Unit> asyncRequest = null;
        asyncRequest = webClient.AuthenticationAsObservable()
            .Catch( ( Exception error ) => Observable.Throw<string>( error ) )
            .AsUnitObservable();

        return asyncRequest.PublishLast().RefCount().Do( _ => logger.DebugFormat( "{0}    =>    {1}\n", this, "done Authenticat" ) );
    }
    #endregion
    //////////////////////////////
}